package com.smartcampus.iitd.smartcampus.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import com.smartcampus.iitd.smartcampus.MainActivity;
import com.smartcampus.iitd.smartcampus.R;
import com.smartcampus.iitd.smartcampus.definitions.Notices;


import java.util.ArrayList;

/**
 * Created by pavan varma on 5/30/2015.
 */
public class saved_list_adapter extends BaseAdapter {
    private static Context mContext;
    private final String TAG="p_adapter";
    private ArrayList<Notices> mItems;
    public saved_list_adapter(ArrayList<Notices> items, Context c)
    {
        mContext=c;
        mItems=items;
        Log.i(TAG, "constructor is called");
    }
    @Override
    public View getView(final int position,View convertView, ViewGroup parent)
    {   Log.i(TAG, "getView is called");
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)   mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.saved_page_list_item, null);
        }
        int postion_from_last=mItems.size()-(position+1);
        Notices CurrentItem=mItems.get(postion_from_last);
        ImageView mImageView=(ImageView)convertView.findViewById(R.id.saved_page_image);
        /*Picasso.with(mContext)
                .load(MainScreenActivity.image_url + CurrentItem.dealId + ".png")
                .into(mImageView);*/
        mImageView.setImageResource(MainActivity.imageresource);
        //mImageView.setImageDrawable(R.drawable.iitd_building);
        TextView mTextView_title=(TextView)convertView.findViewById(R.id.saved_page_title_name);
        TextView mTextView_validity=(TextView)convertView.findViewById(R.id.saved_page_Validity);
        TextView mTextView_description = (TextView) convertView.findViewById(R.id.saved_page_Deal_desc);
        mTextView_title.setText(CurrentItem.cateGory);
        mTextView_validity.setText(CurrentItem.date);
        mTextView_description.setText(CurrentItem.subject);
        return convertView;

    }
    public static int getResourceId(String pVariableName, String pResourcename, String pPackageName)
    {
        try {
            return mContext.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int i) {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

}
