package com.smartcampus.iitd.smartcampus.utilities;


import android.util.Log;

import com.smartcampus.iitd.smartcampus.definitions.SmartResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Naren on 6/27/2015.
 */
public class SmartResponseParser {

    public static SmartResponse parseResponse(String responses)
    {
        SmartResponse smres = new SmartResponse(1,2,"Bus on the Road",15.262626,45.2354);
        try {
            JSONObject arr = new JSONObject(responses);
            /*for (int i=0;i<arr.length();i++)
            {
                JSONObject jsobj = arr.getJSONObject(i);
                alldeals.add(new SmartResponse(jsobj.getInt("timestamp"), jsobj.getInt("number_plate"),
                        jsobj.getString("bus_name"), jsobj.getDouble("lat"), jsobj.getDouble("lon")));
            }*/
            Log.e("Error String inform",arr.getString("info"));
            if(arr.getInt("status")==0)
            {
                Log.e("Some error have occured"," : stay tuned for next update");
            }
            else
            {
                if (arr.getInt("status")==1)
                {
                    Log.e("There was success "," : stay tuned for next update");
                    JSONObject jsobj = arr.getJSONObject("data");
                    smres = new SmartResponse(jsobj.getInt("timestamp"),jsobj.getInt("number_plate"),jsobj.getString("alias"),jsobj.getDouble("lat"),jsobj.getDouble("lon"));

                }
                else
                {
                    Log.e("Pta nahi what happened "," : stay tuned for next update");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return smres;
    }

}
