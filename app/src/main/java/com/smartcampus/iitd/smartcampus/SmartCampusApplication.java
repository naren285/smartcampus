package com.smartcampus.iitd.smartcampus;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.smartcampus.iitd.smartcampus.definitions.Notices;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Naren on 9/13/2015.
 */
public class SmartCampusApplication extends Application {
    private RequestQueue mRequestQueue;
    public ProgressDialog dialog;
    public ArrayList<Notices> mItems = new ArrayList<Notices>();

    /**
     * Log or request TAG
     */
    public static final String TAG = "Smart Campus Application";


    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static SmartCampusApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        mItems.add(new Notices("iitd_building.png","Circular","Date : 20July","Old Campus System is Shutting Down due to Maintenance"));
        mItems.add(new Notices("iitd_building.png","Circular","Date : 20August","Old Campus System is Shutting Down due to Maintenance"));
        mItems.add(new Notices("iitd_building.png","Circular","Date : 20August","Old Campus System is Shutting Down due to Maintenance"));
        mItems.add(new Notices("iitd_building.png","Circular","Date : 20August","Old Campus System is Shutting Down due to Maintenance"));
        mItems.add(new Notices("iitd_building.png","Circular","Date : 20August","Old Campus System is Shutting Down due to Maintenance"));
        mItems.add(new Notices("iitd_building.png","Circular","Date : 20August","Old Campus System is Shutting Down due to Maintenance"));
    }



    public static void ShowToastMessage(String ToastMessage, Context context) {
        Toast toast = Toast.makeText(context, ToastMessage, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }








    /////////
    public static synchronized SmartCampusApplication getInstance() {
        return sInstance;
    }


    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());
            Cache cache = new DiskBasedCache(getApplicationContext().getCacheDir(), 1024 * 1024 * 10); // 1MB cap
            // Instantiate the RequestQueue with the cache and network.
            mRequestQueue = new RequestQueue(cache, network);
            // Start the queue
            mRequestQueue.start();
        }

        return mRequestQueue;
    }


    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        getRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


}
