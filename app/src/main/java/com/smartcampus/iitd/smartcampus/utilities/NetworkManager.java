package com.smartcampus.iitd.smartcampus.utilities;

import android.app.ProgressDialog;
import android.util.Log;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;
import com.smartcampus.iitd.smartcampus.MainActivity;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import com.android.volley.Response;
import com.smartcampus.iitd.smartcampus.SmartCampusApplication;
import com.smartcampus.iitd.smartcampus.definitions.SmartResponse;


import org.json.JSONArray;
import org.json.JSONObject;


/**
 * Created by Naren on 6/27/2015.
 */
public class NetworkManager {
    public static String updateloc_url = "http://nilgiri.subscribo.me:8000/track/updateLoc/";
    public static String getloc_url = "http://nilgiri.subscribo.me:8000/track/getLoc/";
    public static SmartResponse all_response ;
    // Instantiate the cache
    public static String update_req = "PostRequest";
    public static String get_request = "GetRequest";
    // Set up the network to use HttpURLConnection as the HTTP client.
    public static SmartResponse getloc(String url) {
        url = url+'?'+String.format("bus=%d",1);
        Log.e("url is ", url);
        SmartResponse sl = getalllocations(url, get_request);
        return sl;
    }

    public static SmartResponse getalllocations(String url, String Tag)
    {
        /*final ProgressDialog progressDialog = new ProgressDialog(MainActivity.instance);
        progressDialog.setCancelable(false);*/
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (url, new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String res = response.toString();
                        all_response = SmartResponseParser.parseResponse(res);
                        MainActivity.BusCurrentLatLng = new LatLng(all_response.lat,all_response.longti);
                        //return all_response;
                       /* progressDialog.dismiss();*/
                        Log.e("we got response",""+response.toString());


                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volly Error", "Error: " + error.getLocalizedMessage());
                       /* progressDialog.dismiss();*/
                        // TODO Auto-generated method stub

                    }
                });
        SmartCampusApplication.getInstance().addToRequestQueue(jsObjRequest, Tag);
        /*progressDialog.setMessage("Fetching Data....");
        progressDialog.show();*/
        /*while (progressDialog.isShowing())
        {

        }*/
        return all_response;
    }

    public static void updateloc(int busid,int driver, String lat,String lon) {


        String catequery = "";

        catequery = String.format("bus=%d&driver=%d&lat=%s&lon=%s", busid, driver,lat,lon);
        String final_url = updateloc_url+'?'+catequery;
        //updateloc(final_url, update_req);
        //updatereloc(updateloc_url, update_req);
        Log.e("update req:",final_url);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (final_url, new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //String res = response.toString();
                        //all_response = SmartResponseParser.parseResponse(res);
                        Log.e("we got response",""+response.toString());

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volly Error", "Error: " + error.getLocalizedMessage());
                        //progressDialog.dismiss();
                        // TODO Auto-generated method stub

                    }
                });
        SmartCampusApplication.getInstance().addToRequestQueue(jsObjRequest, update_req);
    }




    public static String readIt(InputStream stream) throws IOException {
        //Reader reader = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();
        String contentAsString = sb.toString();
        return contentAsString;
    }



    public static String getallloc(URL myurl) throws IOException {
        // http client
        InputStream is = null;
        HttpURLConnection conn = (HttpURLConnection) myurl.openConnection();
        String charset = "UTF-8";
        conn.setRequestProperty("Accept-Charset", charset);
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        int response = conn.getResponseCode();
        Log.d("Debug Tag: ", "The response is: " + response);
        is = conn.getInputStream();

        String stresponse = readIt(is);
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stresponse;
    }

}
