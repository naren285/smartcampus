package com.smartcampus.iitd.smartcampus;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.smartcampus.iitd.smartcampus.definitions.SmartResponse;
import com.smartcampus.iitd.smartcampus.gdaplibrary.GoogleDirection;
import com.smartcampus.iitd.smartcampus.utilities.DirectionsJSONParser;
import com.smartcampus.iitd.smartcampus.utilities.NetworkManager;

import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Naren on 9/17/2015.
 */
public class MainActivity extends ActionBarActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public static ActionBarActivity instance;
    //public GoogleDirection googledirection = new GoogleDirection(this);
    public int[] rajchu = new int[5];
    public static int imageresource;
    Switch AutoUpdateOnOff;
    public static GoogleApiClient mGoogleApiClient;
    public Location mLastLocation;
    public Location mLastUpdateLocation;
    public static String mLatitudeText;
    public static String mLongitudeText;
    protected String mLastUpdateTimeText;
    public Timer LocationUpdateTimer;
    public static Timer BusUpdateTimer;
    public static LatLng LastBusLatLng ;
    public static LatLng BusCurrentLatLng;
    public static GoogleMap mMap;
    public GoogleDirection gd;
    public Document mDoc;
    public LatLng end;
    public static LatLng start;
    public View mapView;
    public View btnMyLocation;
    public LatLngBounds BusandUserBounds;
    public static MarkerOptions start_marker_option;
    public static MarkerOptions end_marker_option;
    public static Marker bus_marker;
    public static Marker user_marker;
    public static Toast latlongtaost;
    public static Toast buslatlongtoast;
    /**
    * Tracks the status of the location updates request. Value changes when the user presses the
    * Start Updates and Stop Updates buttons.
            */
    protected Boolean mRequestingLocationUpdates = true;
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    public static final float SMALLEST_DISPLACEMENT_IN_METERS = (float)1.0;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;
    /**
     * Represents a geographical location.
     */
    public static Location mCurrentLocation;

    /**
     * Log or request TAG
     */
    public static final String TAG = "Smart Campus Application";

    /**
     * Time when the location was updated represented as a String.
     */
    protected String mLastUpdateTime;

    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            if(user_marker!=null)
            {
                user_marker.setPosition(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
            }
            updateUI();
            Log.e("---", "Just got ready");
            startLocationUpdates();
        }
        if (mLastLocation != null) {
            mLatitudeText = (String.valueOf(mLastLocation.getLatitude()));
            mLongitudeText = (String.valueOf(mLastLocation.getLongitude()));
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int i) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();

    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(SMALLEST_DISPLACEMENT_IN_METERS);
    }

    /**
     * Updates the latitude, the longitude, and the last location time in the UI.
     */
    private void updateUI() {
        if (mCurrentLocation != null) {
            mLatitudeText = (String.valueOf(mCurrentLocation.getLatitude()));
            mLongitudeText = (String.valueOf(mCurrentLocation.getLongitude()));
            mLastUpdateTimeText = (mLastUpdateTime);
        }
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    public void startLocationUpdates() {
        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        Toast.makeText(getApplicationContext(), "started the location service", Toast.LENGTH_SHORT).show();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }


    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.

        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }


    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        Toast.makeText(getApplicationContext(),"abcdefgi",Toast.LENGTH_SHORT).setGravity(Gravity.TOP,50,50);
        Log.e("Location have been changed",location.toString());
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        if(user_marker!=null)
        {
            user_marker.setPosition(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
        }

        updateUI();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buildGoogleApiClient();
        mGoogleApiClient.connect();
        if (mGoogleApiClient.isConnected()) {
            Log.e("abcd", "I started immeditely");
            startLocationUpdates();
        }

        instance = this;
        imageresource = getResources().getIdentifier("com.smartcampus.iitd.smartcampus:drawable/" + "full", null, null);;
        Log.e("Image rs : ", imageresource + ";;;");
        // Getting reference to SupportMapFragment of the activity_main
        SupportMapFragment fm = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        // Getting Map for the SupportMapFragment
        mMap = fm.getMap();
        if (mMap == null) {
            Log.e("kuch error aayega", ": 12345");
        }
        else
        {
            mMap.setMapType(1);
        }
        mMap.setMyLocationEnabled(true);
        if(mCurrentLocation!=null){
            end = new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
        }
        else
        {
            end = new LatLng(28.547824, 77.185501);;
        }
        //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String user_lat = "28.544896";
        String user_lng = "77.189603";
        //   Toast.makeText(getBaseContext(),"shareduser:"+user_lat+"&"+user_lng,Toast.LENGTH_LONG).show();
        NetworkManager.getloc(NetworkManager.getloc_url);
        //BusCurrentLatLng = new LatLng(current_bus_data.lat,current_bus_data.longti);
        start = new LatLng(Double.parseDouble(user_lat), Double.parseDouble(user_lng));
        startBusLocUpdates();
        //to change the position of current location button
        mapView = fm.getView();
        btnMyLocation = ((View) mapView.findViewById(1).getParent()).findViewById(2);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(80, 80); // size of button in dp
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        params.setMargins(0, 0, 30, 30);
        btnMyLocation.setLayoutParams(params);
        BusandUserBounds = new LatLngBounds(
                start, end);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(start, 18));
        BitmapDescriptor bus_icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_directions_bus_black_18dp);
        BitmapDescriptor user_icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_accessibility_black_18dp);
        start_marker_option = new MarkerOptions().position(start).icon(bus_icon).title("Bus");
        end_marker_option = new MarkerOptions().position(end).icon(user_icon).title("User");
        bus_marker = mMap.addMarker(start_marker_option);
        user_marker = mMap.addMarker(end_marker_option);
        /*gd = new GoogleDirection(getApplicationContext());
        gd.setOnDirectionResponseListener(new GoogleDirection.OnDirectionResponseListener() {
            public void onResponse(String status, Document doc, GoogleDirection gd) {
                mDoc = doc;
                mMap.addPolyline(gd.getPolyline(doc, 6, Color.BLUE));
            }
        });
        gd.setLogging(true);
        gd.request(start, end, GoogleDirection.MODE_DRIVING);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    public void checkLocationService() {
        final Context context = getApplicationContext();
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
            builder.setTitle("Location Service Disabled");
            builder.setMessage("Enable Location Service");
            builder.setPositiveButton("Turn Location On", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(myIntent);
                    //get gps
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            builder.show();
        }
    }

    public void isInternetConnected(Context ctx) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        final Context contxt = ctx;
        NetworkInfo wifi = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        // Check if wifi or mobile network is available or not. If any of them is
        // available or connected then it will return true, otherwise false;
        if (wifi != null && mobile != null) {
            if (!wifi.isConnected() && !mobile.isConnected()) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
                builder.setTitle("No Internet Connection");
                builder.setMessage("You are not connected to Internet Connection that is bad :( Please Turn it On");
                builder.setPositiveButton("Turn On", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub
                        Intent myIntent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        contxt.startActivity(myIntent);
                        //get gps
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub

                    }
                });
                builder.show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        AutoUpdateOnOff = (Switch)menu.findItem(R.id.myswitch).getActionView().findViewById(R.id.switchForActionBar);
        AutoUpdateOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Log.e("--->> ", "" + mGoogleApiClient.isConnected());
                    startLocationUpdates();
                    LocationUpdateTimer = new Timer();
                    LocationUpdateTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if(mLastUpdateLocation!=null && mCurrentLocation!=null)
                            {
                                /*String lastlatlong = ""+mLastUpdateLocation.getLatitude()+""+mLastUpdateLocation.getLongitude();
                                if(!lastlatlong.equals(""+mCurrentLocation.getLatitude()+""+mCurrentLocation.getLongitude()))
                                {*/

                                    updateLocationMethod();
                                //}/**/
                            }
                            else
                            {
                                updateLocationMethod();
                            }

                        }

                    }, 0, 15000);

                }
                else
                {
                    turnoffLocUpdate();
                }
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id) {
            case R.id.notices:
                Intent intent = new Intent(this, NoticeBoardActivity.class);
                this.startActivity(intent);
                break;
            case R.id.myswitch:
                // another startActivity, this is for item with id "menu_item2"
                return super.onOptionsItemSelected(item);
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }
    public void updateLocationMethod()
    {
        Log.e("Toggle Button On Click", "click on toggle button");
        if(mCurrentLocation!=null)
        {
            mLastUpdateLocation = mCurrentLocation;
            NetworkManager.updateloc(1, 1, mCurrentLocation.getLatitude()+"", mCurrentLocation.getLongitude()+"");
//            startLocationUpdates();
            this.runOnUiThread(Timer_Tick);

        }
        else
        {
            startLocationUpdates();
        }

    }

    public Runnable Timer_Tick = new Runnable() {
        public void run() {

            //This method runs in the same thread as the UI.

            //Do something to the UI thread here
            latlongtaost = Toast.makeText(getApplicationContext(),mCurrentLocation.getLatitude()+"--"+mCurrentLocation.getLongitude(),Toast.LENGTH_SHORT);
            latlongtaost.show();
        }
    };

    public void turnoffLocUpdate()
    {
        Log.e("Toggle Button off Click", "turn off toggle button");
        LocationUpdateTimer.cancel();
        stopLocationUpdates();
    }


    public static void startBusLocUpdates()
    {
        BusUpdateTimer = new Timer();
        BusUpdateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (BusCurrentLatLng != null) {

                    if (LastBusLatLng == null) {
                        start = BusCurrentLatLng;
                        animate_marker();
                        LastBusLatLng = BusCurrentLatLng;
                    }
                    /*String lastlatlong = ""+LastBusLatLng.latitude+""+LastBusLatLng.longitude;
                    if(!lastlatlong.equals(""+BusCurrentLatLng.latitude+""+BusCurrentLatLng.longitude))
                    {*/
                    start = BusCurrentLatLng;
                    animate_marker();

                    //}
                    NetworkManager.getloc(NetworkManager.getloc_url);
                } else {
                    NetworkManager.getloc(NetworkManager.getloc_url);
                }

            }

        }, 0, 15000);
    }

    public static void animate_marker()
    {
        instance.runOnUiThread(UI_Update_Task);

    }


    public static Runnable UI_Update_Task = new Runnable() {
        public void run() {

            //This method runs in the same thread as the UI.
            buslatlongtoast = Toast.makeText(instance,start.latitude+" /// "+start.longitude,Toast.LENGTH_SHORT);
            buslatlongtoast.setGravity(Gravity.CENTER,50,50);
            buslatlongtoast.getView().setBackgroundColor(Color.GREEN);
            buslatlongtoast.show();
            //Do something to the UI thread here
            bus_marker.setPosition(start);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(start));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
        }
    };

}
