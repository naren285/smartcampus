package com.smartcampus.iitd.smartcampus;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyandroidanimations.library.ShakeAnimation;
import com.smartcampus.iitd.smartcampus.adapters.saved_list_adapter;
import com.smartcampus.iitd.smartcampus.definitions.Notices;
import com.twotoasters.jazzylistview.JazzyListView;
import com.twotoasters.jazzylistview.effects.FlipEffect;

import java.util.ArrayList;

public class NoticeBoardActivity extends ActionBarActivity {
    public ArrayList<Notices> all_notices;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_page);
        JazzyListView mListView = (JazzyListView) findViewById(R.id.save_page_list);
        all_notices = SmartCampusApplication.getInstance().mItems;
//        mDealdetails.addAll(MainActivity.alldeal);
        saved_list_adapter Adapter = new saved_list_adapter(all_notices, getApplicationContext());
        mListView.setTransitionEffect(new FlipEffect());
        mListView.setAdapter(Adapter);
        LinearLayout mLinearLayout = (LinearLayout) findViewById(R.id.no_saved_page);
        if (all_notices.size() == 0) {
                /*LinearLayout mLinearLayout=(LinearLayout)view.findViewById(R.id.no_saved_page);*/
            mLinearLayout.setVisibility(View.VISIBLE);
            TextView tx1 = (TextView) mLinearLayout.findViewById(R.id.tx1);
            //Typeface tf1= Typeface.createFromAsset(MainActivity.instance.getAssets(),"fonts/bariol_regular.otf");
            //tx1.setTypeface(tf1);
            ImageView iv1 = (ImageView) mLinearLayout.findViewById(R.id.image_to_bounce);
            new ShakeAnimation(iv1).setDuration(100).setNumOfShakes(2).animate();
        } else {
            mLinearLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notice_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
