package com.smartcampus.iitd.smartcampus.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.easyandroidanimations.library.ShakeAnimation;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.smartcampus.iitd.smartcampus.MainActivity;
import com.smartcampus.iitd.smartcampus.R;
import com.smartcampus.iitd.smartcampus.SmartCampusApplication;
import com.smartcampus.iitd.smartcampus.adapters.saved_list_adapter;
import com.smartcampus.iitd.smartcampus.definitions.Notices;
import com.smartcampus.iitd.smartcampus.definitions.SmartResponse;
import com.smartcampus.iitd.smartcampus.gdaplibrary.GoogleDirection;
import com.smartcampus.iitd.smartcampus.utilities.NetworkManager;
import com.twotoasters.jazzylistview.JazzyListView;
import com.twotoasters.jazzylistview.effects.FlipEffect;

import org.w3c.dom.Document;

import java.util.ArrayList;

/**
 * Created by Naren on 9/17/2015.
 */
public class PageFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public GoogleMap mMap;
    public GoogleDirection gd;
    public Document mDoc;
    public LatLng end;
    public LatLng start;
    public View view ;
    public int mPage;
    public AppCompatActivity myContext;
    private SupportMapFragment fragment;
    public PageFragment fragment_instance;
    public ArrayList<Notices> all_notices;
    public static android.support.v4.app.FragmentManager fm;

    public static PageFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(AppCompatActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragment_instance = this;
        mPage = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        if (mPage == 1) {

            try {
                view = inflater.inflate(R.layout.activity_simple_direction, container, false);
            } catch (InflateException e) {
        /* map is already there, just return view as it is */
                Log.e("Exception on inflating", e.toString());
            }
            //view = inflater.inflate(R.layout.activity_simple_direction, container, false);
            //MainActivity.isInternetConnected(MainActivity);
            //checkLocationService();
            FragmentManager fm = getChildFragmentManager();
            fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
            if (fragment == null) {
                fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.map, fragment).commit();
            }
            mMap = fragment.getMap();
            if (mMap == null) {
                Log.e("kuch error aayega", ": 12345");
            }
            if(MainActivity.mCurrentLocation!=null){
                end = new LatLng(MainActivity.mCurrentLocation.getLatitude(),MainActivity.mCurrentLocation.getLongitude());
            }
            else
            {
                end = new LatLng(28.547824, 77.185501);
            }

            //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String user_lat = "28.544896";
            String user_lng = "77.189603";
            //   Toast.makeText(getBaseContext(),"shareduser:"+user_lat+"&"+user_lng,Toast.LENGTH_LONG).show();

            start = new LatLng(Double.parseDouble(user_lat), Double.parseDouble(user_lng));
            mMap.setMyLocationEnabled(true);
            //to change the position of current location button
            View mapView = view.findViewById(R.id.map);
            View btnMyLocation = ((View) mapView.findViewById(1).getParent()).findViewById(2);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(80, 80); // size of button in dp
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            params.setMargins(0, 0, 30, 30);
            btnMyLocation.setLayoutParams(params);
            LatLngBounds BusandUserBounds = new LatLngBounds(
                    start, end);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(BusandUserBounds.getCenter(), 17));
            gd = new GoogleDirection(getActivity().getApplicationContext());
            gd.setOnDirectionResponseListener(new GoogleDirection.OnDirectionResponseListener() {
                public void onResponse(String status, Document doc, GoogleDirection gd) {
                    mDoc = doc;
                    mMap.addPolyline(gd.getPolyline(doc, 6, Color.BLUE));
                    mMap.addMarker(new MarkerOptions().position(start)
                            .icon(BitmapDescriptorFactory.defaultMarker(
                                    BitmapDescriptorFactory.HUE_BLUE)));

                    mMap.addMarker(new MarkerOptions().position(end)
                            .icon(BitmapDescriptorFactory.defaultMarker(
                                    BitmapDescriptorFactory.HUE_VIOLET)));
                }
            });
            gd.setLogging(true);
            gd.request(start, end, GoogleDirection.MODE_DRIVING);
            //Toast.makeText(MainActivity.instance, "Getting Directions", Toast.LENGTH_SHORT).show();
        } else {
            if (mPage == 3) {
                view = inflater.inflate(R.layout.fragment_notice_board, container, false);
                final TextView textView = (TextView) view.findViewById(R.id.notice_text);
                textView.setText("Current Loc: Latitude -- " + MainActivity.mLatitudeText + "Longitude -- " + MainActivity.mLongitudeText);
                Button btn = (Button) view.findViewById(R.id.getlocation);
                Button snbtn = (Button) view.findViewById(R.id.sendlocation);
                Button rebtn = (Button) view.findViewById(R.id.resetlocation);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SmartResponse sm = NetworkManager.getloc(NetworkManager.getloc_url);
                        //textView.setText("Response: "+sm.bus_name);

                    }
                });

                rebtn.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        //SmartResponse sm = NetworkManager.getloc(NetworkManager.getloc_url);
                        //textView.setText("Response: "+sm.bus_name);
                        NetworkManager.updateloc(1, 1, 28.5463+"", 77.1915+"");

                    }
                });

                snbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NetworkManager.updateloc(1, 1, (MainActivity.mLatitudeText), (MainActivity.mLongitudeText));
                    }
                });

                //textView.setText("Fragment #" + mPage);
            } else {
                view = inflater.inflate(R.layout.activity_saved_page, container, false);
                //setContentView(R.layout.activity_saved_page);
                fm = getFragmentManager();
                JazzyListView mListView = (JazzyListView) view.findViewById(R.id.save_page_list);
                all_notices = SmartCampusApplication.getInstance().mItems;
//        mDealdetails.addAll(MainActivity.alldeal);
                saved_list_adapter Adapter = new saved_list_adapter(all_notices, getActivity().getApplicationContext());
                mListView.setTransitionEffect(new FlipEffect());
                mListView.setAdapter(Adapter);
                LinearLayout mLinearLayout = (LinearLayout) view.findViewById(R.id.no_saved_page);
                if (all_notices.size() == 0) {
                /*LinearLayout mLinearLayout=(LinearLayout)view.findViewById(R.id.no_saved_page);*/
                    mLinearLayout.setVisibility(View.VISIBLE);
                    TextView tx1 = (TextView) mLinearLayout.findViewById(R.id.tx1);
                    //Typeface tf1= Typeface.createFromAsset(MainActivity.instance.getAssets(),"fonts/bariol_regular.otf");
                    //tx1.setTypeface(tf1);
                    ImageView iv1 = (ImageView) mLinearLayout.findViewById(R.id.image_to_bounce);
                    new ShakeAnimation(iv1).setDuration(100).setNumOfShakes(2).animate();
                } else {
                    mLinearLayout.setVisibility(View.GONE);
                }

            }


        }
        return view;
    }
}
